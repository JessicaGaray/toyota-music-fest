// scrollspy
$(document).ready(function () {
  $('body').scrollspy({
    target:".navbar",offset:50
  });
});

// smooth scroll back to top
$(document).ready(function(){
     $(window).scroll(function () {
            if ($(this).scrollTop() > 50) {
                $('#back-to-top').fadeIn();
            } else {
                $('#back-to-top').fadeOut();
            }
        });
        // scroll body to 0px on click
        $('#back-to-top').click(function () {
            $('#back-to-top').tooltip('hide');
            $('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });

        $('#back-to-top').tooltip('show');

});

// video
$("video").click(function(e){

    // handle click if not Firefox (Firefox supports this feature natively)
    if (typeof InstallTrigger === 'undefined') {

        // get click position
        var clickY = (e.pageY - $(this).offset().top);
        var height = parseFloat( $(this).height() );

        // avoids interference with controls
        if (clickY > 0.82*height) return;

        // toggles play / pause
        this.paused ? this.play() : this.pause();
    }
});


//smooth scroll nav
$('a[href*="#"]')
    .not('[href="#"]')
  .click(function(event) {
      if (
      location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
      &&
      location.hostname == this.hostname
    ) {

      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');

      if (target.length) {

        event.preventDefault();
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000, function() {
          var $target = $(target);
          $target.focus();
          if ($target.is(":focus")) {
            return false;
          } else {
            $target.attr('tabindex','-1');
            $target.focus();
          };
        });
      }
    }
  });
